#include <unistd.h>
#include "stdio.h"
#include "ohos_init.h"

#include "cmsis_os2.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "wifiiot_pwm.h"
#include "wifiiot_adc.h"
#include "wifiiot_i2c.h"
#include "wifiiot_errno.h"
#include "wifiiot_watchdog.h"
//#include "oled_lib.h"

#include "game.h"
#include "bird.h"
#include "ssd1306/ssd1306.h"
#include "lib_btn/dt_btn_util.h"

#define OLED_I2C_BAUDRATE 400*1000

//static int userStart = 0;

static void Button_Callback(const char* sender, BtnEvent event)
{
    printf("[Button] Button_Callback() : %s -> %d\n", sender, event);

    if( event == Pressed )
    { 
        /* Pressed == 1     按下事件处理代码 */ 
        if(isStarted == 0)
        {
            BirdStart();
        }else{
            BirdFlap();
        }
    }
    //if( event == LongPressed ) { /* LongPressed == 2 长按事件处理代码 */ }
    //if( event == Released )    { /* Released == 4    释放事件处理代码 */ }
}

int InitButton(void)
{
    printf("InitButton\n");

    int ret = 0;

    ret += DTButton_Init(); // 初始化按键事件处理上下文

    /* 设置GPIO_8按键的回调函数，同时需要响应按下，释放以及长按三个事件 */
    /* 按键触发顺序： Pressed -> LongPressed(optional) -> Released */
    ret += DTButton_Enable("GPIO_8", Button_Callback, Pressed | LongPressed | Released);

    /* 分别设置S1, S2, USER按键的回调函数 */
    ret += DTButton_Enable("S1", Button_Callback, Pressed | Released);
    ret += DTButton_Enable("S2", Button_Callback, Pressed | LongPressed | Released);
    ret += DTButton_Enable("USR", Button_Callback, LongPressed);

    printf("InitButton: %d\n", ret);
    return ret;
}

void GameTask(void* arg)
{
    (void) arg;
    GpioInit();
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_13, WIFI_IOT_IO_FUNC_GPIO_13_I2C0_SDA);
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_14, WIFI_IOT_IO_FUNC_GPIO_14_I2C0_SCL);
    I2cInit(WIFI_IOT_I2C_IDX_0, OLED_I2C_BAUDRATE);

    WatchDogDisable();

    printf("Hello from Old Boatman\n");

    HAL_Delay(300);//sleep一下确保 app_main后续地代码能顺利执行下去
    ssd1306_Init();
    ssd1306_Fill(Black);
    ssd1306_SetCursor(0, 10);
    ssd1306_DrawString("== Flappy Bird ==\n", Font_7x10, White);
    ssd1306_SetCursor(0, 30);
    ssd1306_DrawString("Press button 1\n", Font_7x10, White);

    InitButton();
	
    uint32_t start = HAL_GetTick();
    ssd1306_UpdateScreen();
    uint32_t end = HAL_GetTick();
    printf("ssd1306_UpdateScreen time cost: %d ms.\r\n", end - start);
	//HAL_Delay(3000);

    //等待用户按键
    while(!isStarted)
    {
        HAL_Delay(300);
    }
	
	BirdInit();
	BirdStart();

    //BirdTest();
    //return;
	
    while (1) {
		//printf("BirdLoop\n");
		
		int isCollision = BirdLoop();
		
		ssd1306_Fill(Black);
		//printf("BirdDrawScreen\n");
		
		BirdDrawScreen();
		
		HAL_Delay(100);

        if(isCollision){
            GameOver();
            
            while(!isStarted)
            {
                HAL_Delay(300);
            }
            BirdInit();
	        BirdStart();
        }
    }
}

void Game(void)
{
    osThreadAttr_t attr;

    attr.name = "GameTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240;
    attr.priority = osPriorityNormal;

    if (osThreadNew(GameTask, NULL, &attr) == NULL) {
        printf("[Game] Falied to create GameTask!\n");
    }
}

SYS_RUN(Game);

